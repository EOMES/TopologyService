﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Oxmes.Topology.Data;

namespace Oxmes.Topology
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    var context = services.GetRequiredService<TopologyContext>();
                    var environment = services.GetRequiredService<IHostingEnvironment>();
                    context.Database.Migrate();

                    if (environment.IsDevelopment()) DbInitialiser.Initialise(context).Wait();

                    var topologies = context.Topologies
                        .Include(t => t.TopologyModuleConveyors)
                        .ThenInclude(tmc => tmc.ModuleConveyor)
                        .Include(t => t.ConveyorLinks)
                        .ThenInclude(cl => cl.DestinationModuleConveyorConveyorLinks)
                        .OrderBy(t => t.TopologyId);
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(topologies);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            var hostConfig = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hosting.json")
                .AddCommandLine(args)
                .Build();

            return WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseConfiguration(hostConfig)
                .UseStartup<Startup>()
                .Build();
        }
    }
}
