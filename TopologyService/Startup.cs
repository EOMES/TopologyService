﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Oxmes.Core.EventBusClient;
using Oxmes.Core.EventBusClient.Enums;
using Oxmes.Core.EventBusClient.Messages.Common;
using Oxmes.Topology.Data;
using Oxmes.Core.ServiceTypes;
using Oxmes.Core.ServiceUtilities;
using System;
using Oxmes.Core.EventBusClient.Exceptions;
using Oxmes.Topology.Interfaces;
using Oxmes.Topology.Services;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System.IO;

namespace Oxmes.Topology
{
    public class Startup
    {
        private readonly ILogger<Startup> Logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            Logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TopologyContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<IServiceClient, ServiceClient>();
            services.AddSingleton<IEventBusClient, EventBusClient>();
            services.AddSingleton<EventBusMessageFactory>();
            services.AddSingleton<IGatewayConnectorService, GatewayConnectorService>();

            services.AddMvc();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Topology Service API", Version = "v1" });

#if DEBUG
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
#endif
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime lifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            lifetime.ApplicationStarted.Register(ApplicationStarted, app.ApplicationServices);
            lifetime.ApplicationStopping.Register(ApplicationStopping, app.ApplicationServices);

            app.UseMvc();

            // Serve Swagger Json
            app.UseSwagger();

            // Enable Swagger UI
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Topology Service API v1");
            });
        }

        private async void ApplicationStarted(object applicationServices)
        {
            if (applicationServices is IServiceProvider services)
            {
                var gatewayConnectorService = services.GetRequiredService<IGatewayConnectorService>();
                gatewayConnectorService.Configure(ConnectionLostPolicy.Reconnect);
                await gatewayConnectorService.ConnectAsync();

                Logger.LogInformation("Initialisation finished");
            }
        }

        private void ApplicationStopping(object applicationServices)
        {
            if (applicationServices is IServiceProvider services)
            {
                Logger.LogInformation("Disconnecting from Event Bus");
                var messageFactory = services.GetRequiredService<EventBusMessageFactory>();
                var eventBusClient = services.GetRequiredService<IEventBusClient>();
                try
                {
                    eventBusClient.CloseAsync().Wait();
                }
                catch (AggregateException ex) when (ex.InnerException.GetType() == typeof(ConnectionLostException))
                {
                }
            }
        }
    }
}