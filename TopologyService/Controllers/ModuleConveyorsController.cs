﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oxmes.Topology.Data;
using Oxmes.Topology.Models;

namespace TopologyService.Controllers
{
    [Produces("application/json")]
    [Route("api/ModuleConveyors")]
    public class ModuleConveyorsController : Controller
    {
        private readonly TopologyContext _context;

        public ModuleConveyorsController(TopologyContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all module conveyors
        /// </summary>
        /// <returns>A list of all module conveyors</returns>
        /// <remarks>
        /// Use GET api/ModuleConveyors/id to retrieve a deep copy of the module conveyor
        ///
        /// Example output:
        ///
        ///     GET /api/ModuleConveyors
        ///     [
        ///         {
        ///             "moduleConveyorId": 0,
        ///             "moduleConveyorName": "unique-name",
        ///             "plcIpAddressString": "10.0.0.144",
        ///             "plcMacAddressString": "0011223344",
        ///             "resourceId": 0
        ///         },
        ///         {
        ///             "moduleConveyorId": 1,
        ///             "moduleConveyorName": "special-name",
        ///             "plcIpAddressString": "10.0.0.145",
        ///             "plcMacAddressString": "0011223355",
        ///             "resourceId": 1
        ///         }
        ///     ]
        /// </remarks>
        // GET: api/ModuleConveyors
        [HttpGet]
        public IEnumerable<ModuleConveyor> GetConveyors()
        {
            return _context.Conveyors;
        }

        /// <summary>
        /// Get a module conveyor by ID
        /// </summary>
        /// <param name="id">The ID of the module conveyor</param>
        /// <returns>A module conveyor</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     GET /api/ModuleConveyors/{<paramref name="id"/>}
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        // GET: api/ModuleConveyors/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetModuleConveyor([FromRoute] int? id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var moduleConveyor = await _context.Conveyors
                .Include(mc => mc.Resource)
                .ThenInclude(r => r.Application)
                .ThenInclude(a => a.SupportedOperations)
                .SingleOrDefaultAsync(m => m.ModuleConveyorId == id);

            if (moduleConveyor == null)
            {
                return NotFound();
            }

            return Ok(moduleConveyor);
        }

        /// <summary>
        /// Update a ModuleConveyor
        /// </summary>
        /// <param name="id">A valid ID of the ModuleConveyor</param>
        /// <param name="moduleConveyor">The ModuleConveyor</param>
        /// <remarks>
        ///
        /// Example of module conveyor:
        ///
        ///     POST /api/ModuleConveyors/{<paramref name="id"/>}
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        // PUT: api/ModuleConveyors/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PutModuleConveyor([FromRoute] int? id, [FromBody] ModuleConveyor moduleConveyor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != moduleConveyor.ModuleConveyorId)
            {
                return BadRequest();
            }

            // Clear reference on old Resource
            var oldResource = _context.Resources.SingleOrDefault(r => r.ModuleConveyorId == id);
            if (oldResource != null) oldResource.ModuleConveyorId = null;
            // Update Module Conveyor
            _context.Entry(moduleConveyor).State = EntityState.Modified;
            // If new Resource is set, update reference for that Resource too
            if (moduleConveyor.ResourceId.HasValue)
            {
                _context.FindAsync<Resource>(moduleConveyor.ResourceId).Result.ModuleConveyor = moduleConveyor;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModuleConveyorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a module conveyor
        /// </summary>
        /// <param name="moduleConveyor">The new module conveyor</param>
        /// <returns>The ModuleConveyor with an ID</returns>
        /// <remarks>
        ///
        /// Example of module conveyor:
        ///
        ///     POST /api/ModuleConveyors
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        // POST: api/ModuleConveyors
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> PostModuleConveyor([FromBody] ModuleConveyor moduleConveyor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Conveyors.Add(moduleConveyor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetModuleConveyor", new { id = moduleConveyor.ModuleConveyorId }, moduleConveyor);
        }

        /// <summary>
        /// Delete a ModuleConveyor
        /// </summary>
        /// <param name="id">A valid ID of the ModuleConveyor to delete</param>
        /// <returns>The deleted ModuleConveyor</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     DELETE /api/ModuleConveyors/{<paramref name="id"/>}
        ///     {
        ///         "moduleConveyorId": 0,
        ///         "moduleConveyorName": "unique-name",
        ///         "plcIpAddressString": "10.0.0.144",
        ///         "plcMacAddressString": "0011223344",
        ///         "resourceId": 0
        ///     }
        /// </remarks>
        // DELETE: api/ModuleConveyors/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteModuleConveyor([FromRoute] int? id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var moduleConveyor = await _context.Conveyors.SingleOrDefaultAsync(m => m.ModuleConveyorId == id);
            if (moduleConveyor == null)
            {
                return NotFound();
            }

            _context.Conveyors.Remove(moduleConveyor);
            await _context.SaveChangesAsync();

            return Ok(moduleConveyor);
        }

        private bool ModuleConveyorExists(int? id)
        {
            return _context.Conveyors.Any(e => e.ModuleConveyorId == id);
        }
    }
}