﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oxmes.Topology.Data;
using Oxmes.Topology.Models;

namespace TopologyService.Controllers
{
    [Produces("application/json")]
    [Route("api/Topologies")]
    public class TopologiesController : Controller
    {
        private readonly TopologyContext Context;

        public TopologiesController(TopologyContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Get a shallow list of Topologies
        /// </summary>
        /// <returns>A shallow list of Topologies</returns>
        /// <remarks>
        /// To speed up response times, this response does not include nested
        /// objects or references
        ///
        /// Example output:
        ///
        ///     GET /api/Topologies
        ///     [
        ///         {
        ///             "topologyId": 0,
        ///             "topologyName": "name",
        ///             "isActive": true,
        ///             "conveyorIds": null,
        ///             "conveyorLinkIds": null
        ///         },
        ///         {
        ///             "topologyId": 1,
        ///             "topologyName": "name2",
        ///             "isActive": false,
        ///             "conveyorIds": null,
        ///             "conveyorLinkIds": null
        ///         }
        ///     ]
        /// </remarks>
        // GET: api/Topologies
        [HttpGet]
        public IEnumerable<Topology> GetTopologies()
        {
            return Context.Topologies;
        }

        /// <summary>
        /// Get a Topology by ID
        /// </summary>
        /// <param name="id">Topology ID</param>
        /// <returns>The Topology with all conveyors and conveyorLinks</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     GET api/Topologies/{<paramref name="id"/>}
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        // GET: api/Topologies/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetTopology([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var topology = await Context.Topologies
                .Include(top => top.TopologyModuleConveyors)
                .ThenInclude(tmc => tmc.ModuleConveyor)
                .Include(top => top.ConveyorLinks)
                .ThenInclude(cl => cl.DestinationModuleConveyorConveyorLinks)
                .SingleOrDefaultAsync(m => m.TopologyId == id);

            if (topology == null)
            {
                return NotFound();
            }

            return Ok(topology);
        }

        /// <summary>
        /// Update Topology
        /// </summary>
        /// <param name="id">ID of the Topology</param>
        /// <param name="topology">The updated Topology</param>
        /// <remarks>
        ///
        /// Example of Topology:
        ///
        ///     PUT api/Topologies/{<paramref name="id"/>}
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        // PUT: api/Topologies/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PutTopology([FromRoute] int id, [FromBody] Topology topology)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != topology.TopologyId)
            {
                return BadRequest();
            }

            topology.ModuleConveyors = topology.ConveyorIds.Select(cid => Context.Conveyors.Find(cid));
            topology.ConveyorLinks = topology.ConveyorLinkIds.Select(linkId =>
                new ConveyorLink
                {
                    ModuleConveyorSource = Context.Conveyors.Find(linkId.Key),
                    ModuleConveyorDestinations = linkId.Value.ToDictionary(kvp => kvp.Key, kvp => Context.Conveyors.Find(kvp.Value))
                });
            Context.Entry(topology).State = EntityState.Modified;

            try
            {
                await Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopologyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create Topology
        /// </summary>
        /// <param name="topology">The Topology</param>
        /// <returns>The Topology with a ID</returns>
        /// <remarks>
        ///
        /// Example Topology/output:
        ///
        ///     POST api/Topologies
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        // POST: api/Topologies
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> PostTopology([FromBody] Topology topology)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            topology.ModuleConveyors = topology.ConveyorIds.Select(id => Context.Conveyors.Find(id));
            topology.ConveyorLinks = topology.ConveyorLinkIds.Select(linkId =>
                new ConveyorLink
                {
                    ModuleConveyorSource = Context.Conveyors.Find(linkId.Key),
                    ModuleConveyorDestinations = linkId.Value.ToDictionary(kvp => kvp.Key, kvp => Context.Conveyors.Find(kvp.Value))
                });

            Context.Topologies.Add(topology);
            await Context.SaveChangesAsync();

            return CreatedAtAction("GetTopology", new { id = topology.TopologyId }, topology);
        }

        /// <summary>
        /// Delete Topology with ID
        /// </summary>
        /// <param name="id">ID of the Topology</param>
        /// <returns>The deleted Topology</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     DELETE api/Topologies/{<paramref name="id"/>}
        ///     {
        ///         "topologyId": 0,
        ///         "topologyName": "name",
        ///         "isActive": true,
        ///         "conveyorIds": [
        ///             0, 1, 2, 3
        ///         ],
        ///         "conveyorLinkIds": {
        ///             "0": {
        ///                 "0": 1
        ///             },
        ///             "1": {
        ///                 "0": 2
        ///             },
        ///             "2": {
        ///                 "0": 3
        ///             },
        ///             "3": {
        ///                 "0": 0
        ///             }
        ///         }
        ///     }
        /// </remarks>
        // DELETE: api/Topologies/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteTopology([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var topology = await Context.Topologies.SingleOrDefaultAsync(m => m.TopologyId == id);
            if (topology == null)
            {
                return NotFound();
            }

            Context.Topologies.Remove(topology);
            await Context.SaveChangesAsync();

            return Ok(topology);
        }

        private bool TopologyExists(int id)
        {
            return Context.Topologies.Any(e => e.TopologyId == id);
        }
    }
}