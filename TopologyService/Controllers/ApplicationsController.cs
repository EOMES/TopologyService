﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oxmes.Topology.Data;
using Oxmes.Topology.Models;

namespace TopologyService.Controllers
{
    [Produces("application/json")]
    [Route("api/Applications")]
    public class ApplicationsController : Controller
    {
        private readonly TopologyContext _context;

        public ApplicationsController(TopologyContext context)
        {
            _context = context;
        }

        
        /// <summary>
        /// Get a shallow list of Applications
        /// </summary>
        /// <returns>A shallow list of Applications</returns>
        /// <remarks>
        /// To speed up response times, this response does include nested objects
        ///
        /// Example output:
        ///
        ///     GET /api/Applications
        ///     [
        ///         {
        ///             "applicationId": 0,
        ///             "applicationName": "name",
        ///             "resourceId": 0,
        ///             "supportedOperations": null
        ///         }
        ///     ]
        ///
        /// </remarks>
        // GET: api/Applications
        [HttpGet]
        public IEnumerable<Application> GetApplications()
        {
            return _context.Applications;
        }

        /// <summary>
        /// Get a deep copy of an Application
        /// </summary>
        /// <param name="id">The ID of the Application to get</param>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     GET /api/Application/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "applicationName": "name",
        ///         "resourceId": 0,
        ///         "supportedOperations": [
        ///             {
        ///                 "applicationOperationId": 0,
        ///                 "operationId": 0,
        ///                 "operationName": "name",
        ///                 "supportedRanges": [
        ///                     {
        ///                         "id": 0,
        ///                         "name": "name",
        ///                         "parameters": [
        ///                             {
        ///                                 "id": 0,
        ///                                 "name": "name",
        ///                                 "minimum": 0,
        ///                                 "maximum": 0,
        ///                                 "minimumFunction": "{X}*2",
        ///                                 "maximumFunction": "{X}*4",
        ///                                 "defaultValue": 0
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        /// <response code="200">Success</response>
        /// <response code="400">Bad Request</response>
        /// <response code="404">No Application with the ID</response>
        // GET: api/Applications/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetApplication([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var application = await _context.Applications
                .Include(a => a.SupportedOperations)
                .ThenInclude(o => o.SupportedRanges)
                .ThenInclude(r => r.Parameters)
                .SingleOrDefaultAsync(m => m.ApplicationId == id);

            if (application == null)
            {
                return NotFound();
            }

            return Ok(application);
        }

        /// <summary>
        /// Update an Application
        /// </summary>
        /// <param name="id">A valid Application ID</param>
        /// <param name="application">The updated Application</param>
        /// <remarks>
        /// Also adds/updates/deletes Operations, Ranges and Parameters
        ///
        /// Example of application:
        ///
        ///     PUT /api/Application/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "applicationName": "name",
        ///         "resourceId": 0,
        ///         "supportedOperations": [
        ///             {
        ///                 "applicationOperationId": 0,
        ///                 "operationId": 0,
        ///                 "operationName": "name",
        ///                 "supportedRanges": [
        ///                     {
        ///                         "id": 0,
        ///                         "name": "name",
        ///                         "parameters": [
        ///                             {
        ///                                 "id": 0,
        ///                                 "name": "name",
        ///                                 "minimum": 0,
        ///                                 "maximum": 0,
        ///                                 "minimumFunction": "{X}*2",
        ///                                 "maximumFunction": "{X}*4",
        ///                                 "defaultValue": 0
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        // PUT: api/Applications/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PutApplication([FromRoute] int id, [FromBody] Application application)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != application.ApplicationId)
            {
                return BadRequest();
            }

            var indexes = new SortedSet<int?>[] { new SortedSet<int?>(), new SortedSet<int?>(), new SortedSet<int?>() };

            foreach (var op in application.SupportedOperations)
            {
                if (op.ApplicationOperationId.HasValue)
                {
                    indexes[0].Add(op.ApplicationOperationId);
                    foreach (var r in op.SupportedRanges)
                    {
                        if (r.Id.HasValue)
                        {
                            indexes[1].Add(r.Id);
                            foreach (var param in r.Parameters)
                            {
                                if (param.Id.HasValue) indexes[2].Add(param.Id);
                            }
                        }
                    }
                }
            }

            _context.Update(application);
            var removedOperations = _context.ApplicationOperations
                .Include(o => o.SupportedRanges)
                .ThenInclude(r => r.Parameters)
                .Where(o => !indexes[0].Contains(o.ApplicationOperationId) && o.ApplicationId == id);
            foreach (var op in removedOperations)
            {
                var ranges = op.SupportedRanges;
                foreach (var range in ranges)
                {
                    _context.RangeParameters.RemoveRange(range.Parameters);
                }
                _context.Ranges.RemoveRange(ranges);
            }
            _context.ApplicationOperations.RemoveRange(removedOperations);

            var removedRanges = _context.Ranges
                .Include(r => r.Parameters)
                .Where(r => !indexes[1].Contains(r.Id) && indexes[0].Contains(r.ApplicationOperationId));
            foreach (var range in removedRanges)
            {
                _context.RangeParameters.RemoveRange(range.Parameters);
            }
            _context.Ranges.RemoveRange(removedRanges);

            var removedRangeParameters = _context.RangeParameters.Where(rp => !indexes[2].Contains(rp.Id) && indexes[1].Contains(rp.RangeId));
            _context.RangeParameters.RemoveRange(removedRangeParameters);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a new Application
        /// </summary>
        /// <param name="application">The Application</param>
        /// <returns>The Application with an ID</returns>
        /// <remarks>
        /// Also adds Operations, Ranges and Parameters
        ///
        /// Example of application:
        ///
        ///     POST /api/Application
        ///     {
        ///         "applicationId": 0,
        ///         "applicationName": "name",
        ///         "resourceId": 0,
        ///         "supportedOperations": [
        ///             {
        ///                 "applicationOperationId": 0,
        ///                 "operationId": 0,
        ///                 "operationName": "name",
        ///                 "supportedRanges": [
        ///                     {
        ///                         "id": 0,
        ///                         "name": "name",
        ///                         "parameters": [
        ///                             {
        ///                                 "id": 0,
        ///                                 "name": "name",
        ///                                 "minimum": 0,
        ///                                 "maximum": 0,
        ///                                 "minimumFunction": "{X}*2",
        ///                                 "maximumFunction": "{X}*4",
        ///                                 "defaultValue": 0
        ///                             }
        ///                         ]
        ///                     }
        ///                 ]
        ///             }
        ///         ]
        ///     }
        /// </remarks>
        // POST: api/Applications
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> PostApplication([FromBody] Application application)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await AddApplicationAsync(application);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetApplication", new { id = application.ApplicationId }, application);
        }

        /// <summary>
        /// Delete the application with id
        /// </summary>
        /// <param name="id">The ID of the application to delete</param>
        /// <returns>The deleted application</returns>
        // DELETE: api/Applications/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteApplication([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var application = await _context.Applications.SingleOrDefaultAsync(m => m.ApplicationId == id);
            if (application == null)
            {
                return NotFound();
            }

            _context.Applications.Remove(application);
            await _context.SaveChangesAsync();

            return Ok(application);
        }

        private bool ApplicationExists(int id)
        {
            return _context.Applications.Any(e => e.ApplicationId == id);
        }

        private async Task AddRangeAsync(Range range)
        {
            await _context.Ranges.AddAsync(range);
            foreach (var param in range.Parameters) param.Range = range;
            await _context.RangeParameters.AddRangeAsync(range.Parameters);
        }

        private async Task AddApplicationOperationAsync(ApplicationOperation applicationOperation)
        {
            await _context.ApplicationOperations.AddAsync(applicationOperation);
            foreach (var r in applicationOperation.SupportedRanges)
            {
                r.ApplicationOperation = applicationOperation;
                await AddRangeAsync(r);
            }
        }

        private async Task AddApplicationAsync(Application application)
        {
            await _context.Applications.AddAsync(application);
            foreach (var op in application.SupportedOperations)
            {
                op.Application = application;
                await AddApplicationOperationAsync(op);
            }
        }
    }
}