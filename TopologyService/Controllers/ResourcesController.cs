﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Oxmes.Topology.Data;
using Oxmes.Topology.Models;

namespace TopologyService.Controllers
{
    [Produces("application/json")]
    [Route("api/Resources")]
    public class ResourcesController : Controller
    {
        private readonly TopologyContext _context;

        public ResourcesController(TopologyContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of all Resources
        /// </summary>
        /// <returns>A list of all Resources</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     GET /api/Resources
        ///     [
        ///         {
        ///             "applicationId": 0,
        ///             "moduleConveyorId": 0,
        ///             "resourceId": 0,
        ///             "resourceName": "Name"
        ///         },
        ///         {
        ///             "applicationId": 1,
        ///             "moduleConveyorId": 2,
        ///             "resourceId": 1,
        ///             "resourceName": "Name"
        ///         }
        ///     ]
        /// </remarks>
        // GET: api/Resources
        [HttpGet]
        public IEnumerable<Resource> GetResources()
        {
            return _context.Resources;
        }

        /// <summary>
        /// Get a Resource by ID
        /// </summary>
        /// <param name="id">The Resource ID</param>
        /// <returns>A Resource</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     GET /api/Resources/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        // GET: api/Resources/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetResource([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var resource = await _context.Resources
                .Include(r => r.Application)
                .SingleOrDefaultAsync(m => m.ResourceId == id);

            if (resource == null)
            {
                return NotFound();
            }

            return Ok(resource);
        }

        /// <summary>
        /// Update a Resource
        /// </summary>
        /// <param name="id">The ID of the Resource to update</param>
        /// <param name="resource">The updated Resource</param>
        /// <remarks>
        /// For more information about the Resource model, see the Topology Service documentation
        ///
        /// Example output:
        ///
        ///     PUT /api/Resources/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        // PUT: api/Resources/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> PutResource([FromRoute] int id, [FromBody] Resource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != resource.ResourceId)
            {
                return BadRequest();
            }

            // Clear reference on old Application
            var oldApplication = _context.Applications.SingleOrDefault(a => a.ResourceId == id);
            if (oldApplication != null) oldApplication.ResourceId = null;
            // Update Resource
            _context.Entry(resource).State = EntityState.Modified;
            // If new Application is set, update reference for that Application too
            if (resource.ApplicationId.HasValue)
            {
                _context.FindAsync<Application>(resource.ApplicationId).Result.Resource = resource;
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ResourceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Create a Resource
        /// </summary>
        /// <param name="resource">The Resource to create</param>
        /// <returns>The created Resource with its ID</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     POST /api/Resources
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        // POST: api/Resources
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> PostResource([FromBody] Resource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Resources.Add(resource);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetResource", new { id = resource.ResourceId }, resource);
        }

        /// <summary>
        /// Delete a Resource
        /// </summary>
        /// <param name="id">The ID of the Resource to delete</param>
        /// <returns>The deleted Resource</returns>
        /// <remarks>
        ///
        /// Example output:
        ///
        ///     DELETE /api/Resources/{<paramref name="id"/>}
        ///     {
        ///         "applicationId": 0,
        ///         "moduleConveyorId": 0,
        ///         "resourceId": 0,
        ///         "resourceName": "Name"
        ///     }
        /// </remarks>
        // DELETE: api/Resources/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> DeleteResource([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var resource = await _context.Resources.SingleOrDefaultAsync(m => m.ResourceId == id);
            if (resource == null)
            {
                return NotFound();
            }

            _context.Resources.Remove(resource);
            await _context.SaveChangesAsync();

            return Ok(resource);
        }

        private bool ResourceExists(int id)
        {
            return _context.Resources.Any(e => e.ResourceId == id);
        }
    }
}