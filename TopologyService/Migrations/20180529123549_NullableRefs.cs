﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Oxmes.Topology.Migrations
{
    public partial class NullableRefs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Resources_ResourceId",
                table: "Applications");

            migrationBuilder.DropForeignKey(
                name: "FK_Resources_Conveyors_ModuleConveyorId",
                table: "Resources");

            migrationBuilder.AlterColumn<int>(
                name: "ModuleConveyorId",
                table: "Resources",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ResourceId",
                table: "Applications",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Resources_ResourceId",
                table: "Applications",
                column: "ResourceId",
                principalTable: "Resources",
                principalColumn: "ResourceId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Resources_Conveyors_ModuleConveyorId",
                table: "Resources",
                column: "ModuleConveyorId",
                principalTable: "Conveyors",
                principalColumn: "ModuleConveyorId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Resources_ResourceId",
                table: "Applications");

            migrationBuilder.DropForeignKey(
                name: "FK_Resources_Conveyors_ModuleConveyorId",
                table: "Resources");

            migrationBuilder.AlterColumn<int>(
                name: "ModuleConveyorId",
                table: "Resources",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ResourceId",
                table: "Applications",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Resources_ResourceId",
                table: "Applications",
                column: "ResourceId",
                principalTable: "Resources",
                principalColumn: "ResourceId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Resources_Conveyors_ModuleConveyorId",
                table: "Resources",
                column: "ModuleConveyorId",
                principalTable: "Conveyors",
                principalColumn: "ModuleConveyorId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
