﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Oxmes.Topology.Migrations
{
    public partial class ReverseReferences : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OperationId",
                table: "ApplicationOperations",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "OperationId",
                table: "ApplicationOperations",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}