﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Oxmes.Topology.Migrations
{
    public partial class ResourceName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Application_Resource_ResourceId",
                table: "Application");

            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationOperation_Application_ApplicationId",
                table: "ApplicationOperation");

            migrationBuilder.DropForeignKey(
                name: "FK_Resource_Conveyors_ModuleConveyorId",
                table: "Resource");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Resource",
                table: "Resource");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Application",
                table: "Application");

            migrationBuilder.RenameTable(
                name: "Resource",
                newName: "Resources");

            migrationBuilder.RenameTable(
                name: "Application",
                newName: "Applications");

            migrationBuilder.RenameIndex(
                name: "IX_Resource_ModuleConveyorId",
                table: "Resources",
                newName: "IX_Resources_ModuleConveyorId");

            migrationBuilder.RenameIndex(
                name: "IX_Application_ResourceId",
                table: "Applications",
                newName: "IX_Applications_ResourceId");

            migrationBuilder.AddColumn<string>(
                name: "ResourceName",
                table: "Resources",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Resources",
                table: "Resources",
                column: "ResourceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Applications",
                table: "Applications",
                column: "ApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationOperation_Applications_ApplicationId",
                table: "ApplicationOperation",
                column: "ApplicationId",
                principalTable: "Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Applications_Resources_ResourceId",
                table: "Applications",
                column: "ResourceId",
                principalTable: "Resources",
                principalColumn: "ResourceId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Resources_Conveyors_ModuleConveyorId",
                table: "Resources",
                column: "ModuleConveyorId",
                principalTable: "Conveyors",
                principalColumn: "ModuleConveyorId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationOperation_Applications_ApplicationId",
                table: "ApplicationOperation");

            migrationBuilder.DropForeignKey(
                name: "FK_Applications_Resources_ResourceId",
                table: "Applications");

            migrationBuilder.DropForeignKey(
                name: "FK_Resources_Conveyors_ModuleConveyorId",
                table: "Resources");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Resources",
                table: "Resources");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Applications",
                table: "Applications");

            migrationBuilder.DropColumn(
                name: "ResourceName",
                table: "Resources");

            migrationBuilder.RenameTable(
                name: "Resources",
                newName: "Resource");

            migrationBuilder.RenameTable(
                name: "Applications",
                newName: "Application");

            migrationBuilder.RenameIndex(
                name: "IX_Resources_ModuleConveyorId",
                table: "Resource",
                newName: "IX_Resource_ModuleConveyorId");

            migrationBuilder.RenameIndex(
                name: "IX_Applications_ResourceId",
                table: "Application",
                newName: "IX_Application_ResourceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Resource",
                table: "Resource",
                column: "ResourceId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Application",
                table: "Application",
                column: "ApplicationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Application_Resource_ResourceId",
                table: "Application",
                column: "ResourceId",
                principalTable: "Resource",
                principalColumn: "ResourceId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationOperation_Application_ApplicationId",
                table: "ApplicationOperation",
                column: "ApplicationId",
                principalTable: "Application",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Resource_Conveyors_ModuleConveyorId",
                table: "Resource",
                column: "ModuleConveyorId",
                principalTable: "Conveyors",
                principalColumn: "ModuleConveyorId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
