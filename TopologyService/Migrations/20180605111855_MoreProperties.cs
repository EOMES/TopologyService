﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Oxmes.Topology.Migrations
{
    public partial class MoreProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationOperation_Applications_ApplicationId",
                table: "ApplicationOperation");

            migrationBuilder.DropForeignKey(
                name: "FK_Range_ApplicationOperation_ApplicationOperationId",
                table: "Range");

            migrationBuilder.DropForeignKey(
                name: "FK_RangeParameter_Range_RangeId",
                table: "RangeParameter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RangeParameter",
                table: "RangeParameter");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Range",
                table: "Range");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationOperation",
                table: "ApplicationOperation");

            migrationBuilder.RenameTable(
                name: "RangeParameter",
                newName: "RangeParameters");

            migrationBuilder.RenameTable(
                name: "Range",
                newName: "Ranges");

            migrationBuilder.RenameTable(
                name: "ApplicationOperation",
                newName: "ApplicationOperations");

            migrationBuilder.RenameIndex(
                name: "IX_RangeParameter_RangeId",
                table: "RangeParameters",
                newName: "IX_RangeParameters_RangeId");

            migrationBuilder.RenameIndex(
                name: "IX_Range_ApplicationOperationId",
                table: "Ranges",
                newName: "IX_Ranges_ApplicationOperationId");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationOperation_ApplicationId",
                table: "ApplicationOperations",
                newName: "IX_ApplicationOperations_ApplicationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RangeParameters",
                table: "RangeParameters",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Ranges",
                table: "Ranges",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationOperations",
                table: "ApplicationOperations",
                column: "ApplicationOperationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationOperations_Applications_ApplicationId",
                table: "ApplicationOperations",
                column: "ApplicationId",
                principalTable: "Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RangeParameters_Ranges_RangeId",
                table: "RangeParameters",
                column: "RangeId",
                principalTable: "Ranges",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ranges_ApplicationOperations_ApplicationOperationId",
                table: "Ranges",
                column: "ApplicationOperationId",
                principalTable: "ApplicationOperations",
                principalColumn: "ApplicationOperationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationOperations_Applications_ApplicationId",
                table: "ApplicationOperations");

            migrationBuilder.DropForeignKey(
                name: "FK_RangeParameters_Ranges_RangeId",
                table: "RangeParameters");

            migrationBuilder.DropForeignKey(
                name: "FK_Ranges_ApplicationOperations_ApplicationOperationId",
                table: "Ranges");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Ranges",
                table: "Ranges");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RangeParameters",
                table: "RangeParameters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ApplicationOperations",
                table: "ApplicationOperations");

            migrationBuilder.RenameTable(
                name: "Ranges",
                newName: "Range");

            migrationBuilder.RenameTable(
                name: "RangeParameters",
                newName: "RangeParameter");

            migrationBuilder.RenameTable(
                name: "ApplicationOperations",
                newName: "ApplicationOperation");

            migrationBuilder.RenameIndex(
                name: "IX_Ranges_ApplicationOperationId",
                table: "Range",
                newName: "IX_Range_ApplicationOperationId");

            migrationBuilder.RenameIndex(
                name: "IX_RangeParameters_RangeId",
                table: "RangeParameter",
                newName: "IX_RangeParameter_RangeId");

            migrationBuilder.RenameIndex(
                name: "IX_ApplicationOperations_ApplicationId",
                table: "ApplicationOperation",
                newName: "IX_ApplicationOperation_ApplicationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Range",
                table: "Range",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RangeParameter",
                table: "RangeParameter",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ApplicationOperation",
                table: "ApplicationOperation",
                column: "ApplicationOperationId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationOperation_Applications_ApplicationId",
                table: "ApplicationOperation",
                column: "ApplicationId",
                principalTable: "Applications",
                principalColumn: "ApplicationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Range_ApplicationOperation_ApplicationOperationId",
                table: "Range",
                column: "ApplicationOperationId",
                principalTable: "ApplicationOperation",
                principalColumn: "ApplicationOperationId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RangeParameter_Range_RangeId",
                table: "RangeParameter",
                column: "RangeId",
                principalTable: "Range",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
