﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Oxmes.Topology.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Conveyors",
                columns: table => new
                {
                    ModuleConveyorId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Conveyors", x => x.ModuleConveyorId);
                });

            migrationBuilder.CreateTable(
                name: "Topologies",
                columns: table => new
                {
                    TopologyId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Topologies", x => x.TopologyId);
                });

            migrationBuilder.CreateTable(
                name: "Resource",
                columns: table => new
                {
                    ResourceId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ModuleConveyorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resource", x => x.ResourceId);
                    table.ForeignKey(
                        name: "FK_Resource_Conveyors_ModuleConveyorId",
                        column: x => x.ModuleConveyorId,
                        principalTable: "Conveyors",
                        principalColumn: "ModuleConveyorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ConveyorLink",
                columns: table => new
                {
                    LinkId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ModuleConveyorSourceId = table.Column<int>(nullable: false),
                    TopologyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ConveyorLink", x => x.LinkId);
                    table.ForeignKey(
                        name: "FK_ConveyorLink_Conveyors_ModuleConveyorSourceId",
                        column: x => x.ModuleConveyorSourceId,
                        principalTable: "Conveyors",
                        principalColumn: "ModuleConveyorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ConveyorLink_Topologies_TopologyId",
                        column: x => x.TopologyId,
                        principalTable: "Topologies",
                        principalColumn: "TopologyId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TopologyModuleConveyor",
                columns: table => new
                {
                    ModuleConveyorId = table.Column<int>(nullable: false),
                    TopologyId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopologyModuleConveyor", x => new { x.ModuleConveyorId, x.TopologyId });
                    table.ForeignKey(
                        name: "FK_TopologyModuleConveyor_Conveyors_ModuleConveyorId",
                        column: x => x.ModuleConveyorId,
                        principalTable: "Conveyors",
                        principalColumn: "ModuleConveyorId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopologyModuleConveyor_Topologies_TopologyId",
                        column: x => x.TopologyId,
                        principalTable: "Topologies",
                        principalColumn: "TopologyId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Application",
                columns: table => new
                {
                    ApplicationId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApplicationName = table.Column<string>(nullable: true),
                    ResourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Application", x => x.ApplicationId);
                    table.ForeignKey(
                        name: "FK_Application_Resource_ResourceId",
                        column: x => x.ResourceId,
                        principalTable: "Resource",
                        principalColumn: "ResourceId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ModuleConveyorConveyorLink",
                columns: table => new
                {
                    ModuleConveyorId = table.Column<int>(nullable: false),
                    ConveyorLinkId = table.Column<int>(nullable: false),
                    PortNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleConveyorConveyorLink", x => new { x.ModuleConveyorId, x.ConveyorLinkId });
                    table.ForeignKey(
                        name: "FK_ModuleConveyorConveyorLink_ConveyorLink_ConveyorLinkId",
                        column: x => x.ConveyorLinkId,
                        principalTable: "ConveyorLink",
                        principalColumn: "LinkId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleConveyorConveyorLink_Conveyors_ModuleConveyorId",
                        column: x => x.ModuleConveyorId,
                        principalTable: "Conveyors",
                        principalColumn: "ModuleConveyorId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationOperation",
                columns: table => new
                {
                    ApplicationOperationId = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApplicationId = table.Column<int>(nullable: true),
                    OperationId = table.Column<int>(nullable: false),
                    OperationName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationOperation", x => x.ApplicationOperationId);
                    table.ForeignKey(
                        name: "FK_ApplicationOperation_Application_ApplicationId",
                        column: x => x.ApplicationId,
                        principalTable: "Application",
                        principalColumn: "ApplicationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Range",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ApplicationOperationId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Range", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Range_ApplicationOperation_ApplicationOperationId",
                        column: x => x.ApplicationOperationId,
                        principalTable: "ApplicationOperation",
                        principalColumn: "ApplicationOperationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RangeParameter",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    DefaultValue = table.Column<double>(nullable: true),
                    Maximum = table.Column<double>(nullable: true),
                    MaximumFunction = table.Column<string>(nullable: true),
                    Minimum = table.Column<double>(nullable: true),
                    MinimumFunction = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    RangeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RangeParameter", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RangeParameter_Range_RangeId",
                        column: x => x.RangeId,
                        principalTable: "Range",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Application_ResourceId",
                table: "Application",
                column: "ResourceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationOperation_ApplicationId",
                table: "ApplicationOperation",
                column: "ApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_ConveyorLink_ModuleConveyorSourceId",
                table: "ConveyorLink",
                column: "ModuleConveyorSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_ConveyorLink_TopologyId",
                table: "ConveyorLink",
                column: "TopologyId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleConveyorConveyorLink_ConveyorLinkId",
                table: "ModuleConveyorConveyorLink",
                column: "ConveyorLinkId");

            migrationBuilder.CreateIndex(
                name: "IX_Range_ApplicationOperationId",
                table: "Range",
                column: "ApplicationOperationId");

            migrationBuilder.CreateIndex(
                name: "IX_RangeParameter_RangeId",
                table: "RangeParameter",
                column: "RangeId");

            migrationBuilder.CreateIndex(
                name: "IX_Resource_ModuleConveyorId",
                table: "Resource",
                column: "ModuleConveyorId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TopologyModuleConveyor_TopologyId",
                table: "TopologyModuleConveyor",
                column: "TopologyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModuleConveyorConveyorLink");

            migrationBuilder.DropTable(
                name: "RangeParameter");

            migrationBuilder.DropTable(
                name: "TopologyModuleConveyor");

            migrationBuilder.DropTable(
                name: "ConveyorLink");

            migrationBuilder.DropTable(
                name: "Range");

            migrationBuilder.DropTable(
                name: "Topologies");

            migrationBuilder.DropTable(
                name: "ApplicationOperation");

            migrationBuilder.DropTable(
                name: "Application");

            migrationBuilder.DropTable(
                name: "Resource");

            migrationBuilder.DropTable(
                name: "Conveyors");
        }
    }
}
