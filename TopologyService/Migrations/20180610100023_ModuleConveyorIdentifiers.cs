﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Oxmes.Topology.Migrations
{
    public partial class ModuleConveyorIdentifiers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ModuleConveyorName",
                table: "Conveyors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PlcIpAddressString",
                table: "Conveyors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PlcMacAddressString",
                table: "Conveyors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ModuleConveyorName",
                table: "Conveyors");

            migrationBuilder.DropColumn(
                name: "PlcIpAddressString",
                table: "Conveyors");

            migrationBuilder.DropColumn(
                name: "PlcMacAddressString",
                table: "Conveyors");
        }
    }
}