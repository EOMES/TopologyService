﻿using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Oxmes.Topology.Models;

namespace Oxmes.Topology.Data
{
    public class TopologyContext : DbContext
    {
        public DbSet<Models.Topology> Topologies { get; set; }
        public DbSet<ModuleConveyor> Conveyors { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<ApplicationOperation> ApplicationOperations { get; set; }
        public DbSet<Range> Ranges { get; set; }
        public DbSet<RangeParameter> RangeParameters { get; set; }

        public TopologyContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TopologyModuleConveyor>()
                .HasKey(tmc => new { tmc.ModuleConveyorId, tmc.TopologyId });

            modelBuilder.Entity<TopologyModuleConveyor>()
                .HasOne(tmc => tmc.Topology)
                .WithMany(t => t.TopologyModuleConveyors)
                .HasForeignKey(tmc => tmc.TopologyId);

            modelBuilder.Entity<TopologyModuleConveyor>()
                .HasOne(tmc => tmc.ModuleConveyor)
                .WithMany(mc => mc.Topologies)
                .HasForeignKey(tmc => tmc.ModuleConveyorId);

            modelBuilder.Entity<ConveyorLink>()
                .HasOne(cl => cl.ModuleConveyorSource)
                .WithMany(mc => mc.OutgoingConveyorLinks);

            modelBuilder.Entity<ModuleConveyorConveyorLink>()
                .HasKey(mccl => new { mccl.ModuleConveyorId, mccl.ConveyorLinkId });

            modelBuilder.Entity<ModuleConveyorConveyorLink>()
                .HasOne(mccl => mccl.ConveyorLink)
                .WithMany(cl => cl.DestinationModuleConveyorConveyorLinks)
                .HasForeignKey(mccl => mccl.ConveyorLinkId);

            modelBuilder.Entity<ModuleConveyorConveyorLink>()
                .HasOne(mccl => mccl.ModuleConveyor)
                .WithMany(mc => mc.IncomingConveyorLinks)
                .HasForeignKey(mccl => mccl.ModuleConveyorId);

            //modelBuilder.Entity<ModuleConveyor>().ToTable("module");

            //modelBuilder.Entity<ModuleNeighbourPair>()
            //    .HasKey(k => new { k.ThisModuleId, k.OtherModuleId });

            //modelBuilder.Entity<ModuleNeighbourPair>()
            //    .HasOne(m => m.ThisModule)
            //    .WithMany(m => m.NeighbourModules)
            //    .HasForeignKey(m => m.ThisModuleId);

            //modelBuilder.Entity<ResourceResource>()
            //    .HasKey(k => new { k.ResourceId, k.NextResourceId });

            //modelBuilder.Entity<ResourceResource>()
            //    .HasOne(r => r.Resource)
            //    .WithMany(r => r.NextResources)
            //    .HasForeignKey(r => r.ResourceId);
        }
    }
}