﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oxmes.Topology.Data
{
    public class DbInitialiser
    {
        public static async Task Initialise(TopologyContext context)
        {
            if (!context.Topologies.Any())
            {
                // Demo topology
                //             -------
                // Module1 (| m1s | m1n |)
                // Module2 (| m2s | m2n |)
                // Module3 (| m3s | m3n |)
                //             -------

                var module1North = new Models.ModuleConveyor();
                var module1South = new Models.ModuleConveyor();
                var module2North = new Models.ModuleConveyor();
                var module2South = new Models.ModuleConveyor();
                var module3North = new Models.ModuleConveyor();
                var module3South = new Models.ModuleConveyor();

                var resource1 = new Models.Resource { Application = new Models.Application { ApplicationName = "Application1" }, };
                var resource2 = new Models.Resource { Application = new Models.Application { ApplicationName = "Application2" }, };
                var resource3 = new Models.Resource { Application = new Models.Application { ApplicationName = "Application3" }, };
                var resource4 = new Models.Resource { Application = new Models.Application { ApplicationName = "Application4" }, };
                var resource5 = new Models.Resource { Application = new Models.Application { ApplicationName = "Application5" }, };
                var resource6 = new Models.Resource { Application = new Models.Application { ApplicationName = "Application6" }, };

                ((ICollection<Models.ApplicationOperation>)resource1.Application.SupportedOperations).Add(new Models.ApplicationOperation { SupportedRanges = new List<Models.Range> { new Models.Range { Name = "Default" } } });

                module1North.Resource = resource1;
                module1South.Resource = resource4;
                module2North.Resource = resource2;
                module2South.Resource = resource5;
                module3North.Resource = resource3;
                module3South.Resource = resource6;

                var topology = new Models.Topology();
                topology.AddConveyorLink(module1North, module1South, 0);
                topology.AddConveyorLink(module1South, module2South, 0);
                topology.AddConveyorLink(module2South, module3South, 0);
                topology.AddConveyorLink(module3South, module3North, 0);
                topology.AddConveyorLink(module3North, module2North, 0);
                topology.AddConveyorLink(module2North, module1North, 0);

                var topology2 = new Models.Topology();
                topology2.AddConveyorLink(module1North, module1South, 0);
                topology2.AddConveyorLink(module1South, module2South, 0);
                topology2.AddConveyorLink(module2South, module3South, 0);
                topology2.AddConveyorLink(module3South, module3North, 0);
                topology2.AddConveyorLink(module3North, module2North, 0);
                topology2.AddConveyorLink(module2North, module1North, 1);

                await context.AddRangeAsync(new Models.ModuleConveyor[] { module1North, module1South, module2North, module2South, module3North, module3South });
                await context.SaveChangesAsync();
                await context.AddAsync(topology);
                await context.AddAsync(topology2);
                await context.SaveChangesAsync();
            }
        }
    }
}