# Topology Service API


<a name="overview"></a>
## Overview

### Version information
*Version* : v1




<a name="paths"></a>
## Paths

<a name="apiapplicationspost"></a>
### Create a new Application
```
POST /api/Applications
```


#### Description
Also adds Operations, Ranges and Parameters
            
Example of application:
            
    POST /api/Application
    {
        "applicationId": 0,
        "applicationName": "name",
        "resourceId": 0,
        "supportedOperations": [
            {
                "applicationOperationId": 0,
                "operationId": 0,
                "operationName": "name",
                "supportedRanges": [
                    {
                        "id": 0,
                        "name": "name",
                        "parameters": [
                            {
                                "id": 0,
                                "name": "name",
                                "minimum": 0,
                                "maximum": 0,
                                "minimumFunction": "{X}*2",
                                "maximumFunction": "{X}*4",
                                "defaultValue": 0
                            }
                        ]
                    }
                ]
            }
        ]
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**application**  <br>*optional*|The Application|[Application](#application)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Applications


<a name="apiapplicationsget"></a>
### Get a shallow list of Applications
```
GET /api/Applications
```


#### Description
To speed up response times, this response does include nested objects
            
Example output:
            
    GET /api/Applications
    [
        {
            "applicationId": 0,
            "applicationName": "name",
            "resourceId": 0,
            "supportedOperations": null
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|< [Application](#application) > array|


#### Produces

* `application/json`


#### Tags

* Applications


<a name="apiapplicationsbyidget"></a>
### Get a deep copy of an Application
```
GET /api/Applications/{id}
```


#### Description
Example output:
            
    GET /api/Application/{id}
    {
        "applicationId": 0,
        "applicationName": "name",
        "resourceId": 0,
        "supportedOperations": [
            {
                "applicationOperationId": 0,
                "operationId": 0,
                "operationName": "name",
                "supportedRanges": [
                    {
                        "id": 0,
                        "name": "name",
                        "parameters": [
                            {
                                "id": 0,
                                "name": "name",
                                "minimum": 0,
                                "maximum": 0,
                                "minimumFunction": "{X}*2",
                                "maximumFunction": "{X}*4",
                                "defaultValue": 0
                            }
                        ]
                    }
                ]
            }
        ]
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the Application to get|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|No Application with the ID|No Content|


#### Tags

* Applications


<a name="apiapplicationsbyidput"></a>
### Update an Application
```
PUT /api/Applications/{id}
```


#### Description
Also adds/updates/deletes Operations, Ranges and Parameters
            
Example of application:
            
    PUT /api/Application/{id}
    {
        "applicationId": 0,
        "applicationName": "name",
        "resourceId": 0,
        "supportedOperations": [
            {
                "applicationOperationId": 0,
                "operationId": 0,
                "operationName": "name",
                "supportedRanges": [
                    {
                        "id": 0,
                        "name": "name",
                        "parameters": [
                            {
                                "id": 0,
                                "name": "name",
                                "minimum": 0,
                                "maximum": 0,
                                "minimumFunction": "{X}*2",
                                "maximumFunction": "{X}*4",
                                "defaultValue": 0
                            }
                        ]
                    }
                ]
            }
        ]
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|A valid Application ID|integer (int32)|
|**Body**|**application**  <br>*optional*|The updated Application|[Application](#application)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Applications


<a name="apiapplicationsbyiddelete"></a>
### Delete the application with id
```
DELETE /api/Applications/{id}
```


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the application to delete|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Tags

* Applications


<a name="apimoduleconveyorspost"></a>
### Create a module conveyor
```
POST /api/ModuleConveyors
```


#### Description
Example of module conveyor:
            
    POST /api/ModuleConveyors
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**moduleConveyor**  <br>*optional*|The new module conveyor|[ModuleConveyor](#moduleconveyor)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsget"></a>
### Get all module conveyors
```
GET /api/ModuleConveyors
```


#### Description
Use GET api/ModuleConveyors/id to retrieve a deep copy of the module conveyor
            
Example output:
            
    GET /api/ModuleConveyors
    [
        {
            "moduleConveyorId": 0,
            "moduleConveyorName": "unique-name",
            "plcIpAddressString": "10.0.0.144",
            "plcMacAddressString": "0011223344",
            "resourceId": 0
        },
        {
            "moduleConveyorId": 1,
            "moduleConveyorName": "special-name",
            "plcIpAddressString": "10.0.0.145",
            "plcMacAddressString": "0011223355",
            "resourceId": 1
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|< [ModuleConveyor](#moduleconveyor) > array|


#### Produces

* `application/json`


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsbyidget"></a>
### Get a module conveyor by ID
```
GET /api/ModuleConveyors/{id}
```


#### Description
Example output:
            
    GET /api/ModuleConveyors/{id}
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the module conveyor|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsbyidput"></a>
### Update a ModuleConveyor
```
PUT /api/ModuleConveyors/{id}
```


#### Description
Example of module conveyor:
            
    POST /api/ModuleConveyors/{id}
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|A valid ID of the ModuleConveyor|integer (int32)|
|**Body**|**moduleConveyor**  <br>*optional*|The ModuleConveyor|[ModuleConveyor](#moduleconveyor)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* ModuleConveyors


<a name="apimoduleconveyorsbyiddelete"></a>
### Delete a ModuleConveyor
```
DELETE /api/ModuleConveyors/{id}
```


#### Description
Example output:
            
    DELETE /api/ModuleConveyors/{id}
    {
        "moduleConveyorId": 0,
        "moduleConveyorName": "unique-name",
        "plcIpAddressString": "10.0.0.144",
        "plcMacAddressString": "0011223344",
        "resourceId": 0
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|A valid ID of the ModuleConveyor to delete|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Tags

* ModuleConveyors


<a name="apiresourcespost"></a>
### Create a Resource
```
POST /api/Resources
```


#### Description
Example output:
            
    POST /api/Resources
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**resource**  <br>*optional*|The Resource to create|[Resource](#resource)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Resources


<a name="apiresourcesget"></a>
### Get a list of all Resources
```
GET /api/Resources
```


#### Description
Example output:
            
    GET /api/Resources
    [
        {
            "applicationId": 0,
            "moduleConveyorId": 0,
            "resourceId": 0,
            "resourceName": "Name"
        },
        {
            "applicationId": 1,
            "moduleConveyorId": 2,
            "resourceId": 1,
            "resourceName": "Name"
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|< [Resource](#resource) > array|


#### Produces

* `application/json`


#### Tags

* Resources


<a name="apiresourcesbyidget"></a>
### Get a Resource by ID
```
GET /api/Resources/{id}
```


#### Description
Example output:
            
    GET /api/Resources/{id}
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The Resource ID|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Tags

* Resources


<a name="apiresourcesbyidput"></a>
### Update a Resource
```
PUT /api/Resources/{id}
```


#### Description
For more information about the Resource model, see the Topology Service documentation
            
Example output:
            
    PUT /api/Resources/{id}
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the Resource to update|integer (int32)|
|**Body**|**resource**  <br>*optional*|The updated Resource|[Resource](#resource)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Resources


<a name="apiresourcesbyiddelete"></a>
### Delete a Resource
```
DELETE /api/Resources/{id}
```


#### Description
Example output:
            
    DELETE /api/Resources/{id}
    {
        "applicationId": 0,
        "moduleConveyorId": 0,
        "resourceId": 0,
        "resourceName": "Name"
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|The ID of the Resource to delete|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Tags

* Resources


<a name="apitopologiespost"></a>
### Create Topology
```
POST /api/Topologies
```


#### Description
Example Topology/output:
            
    POST api/Topologies
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Body**|**topology**  <br>*optional*|The Topology|[Topology](#topology)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**201**|Success|No Content|
|**400**|Bad Request|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Topologies


<a name="apitopologiesget"></a>
### Get a shallow list of Topologies
```
GET /api/Topologies
```


#### Description
To speed up response times, this response does not include nested
objects or references
            
Example output:
            
    GET /api/Topologies
    [
        {
            "topologyId": 0,
            "topologyName": "name",
            "isActive": true,
            "conveyorIds": null,
            "conveyorLinkIds": null
        },
        {
            "topologyId": 1,
            "topologyName": "name2",
            "isActive": false,
            "conveyorIds": null,
            "conveyorLinkIds": null
        }
    ]


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|< [Topology](#topology) > array|


#### Produces

* `application/json`


#### Tags

* Topologies


<a name="apitopologiesbyidget"></a>
### Get a Topology by ID
```
GET /api/Topologies/{id}
```


#### Description
Example output:
            
    GET api/Topologies/{id}
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|Topology ID|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Tags

* Topologies


<a name="apitopologiesbyidput"></a>
### Update Topology
```
PUT /api/Topologies/{id}
```


#### Description
Example of Topology:
            
    PUT api/Topologies/{id}
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|ID of the Topology|integer (int32)|
|**Body**|**topology**  <br>*optional*|The updated Topology|[Topology](#topology)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**204**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Consumes

* `application/json-patch+json`
* `application/json`
* `text/json`
* `application/*+json`


#### Tags

* Topologies


<a name="apitopologiesbyiddelete"></a>
### Delete Topology with ID
```
DELETE /api/Topologies/{id}
```


#### Description
Example output:
            
    DELETE api/Topologies/{id}
    {
        "topologyId": 0,
        "topologyName": "name",
        "isActive": true,
        "conveyorIds": [
            0, 1, 2, 3
        ],
        "conveyorLinkIds": {
            "0": {
                "0": 1
            },
            "1": {
                "0": 2
            },
            "2": {
                "0": 3
            },
            "3": {
                "0": 0
            }
        }
    }


#### Parameters

|Type|Name|Description|Schema|
|---|---|---|---|
|**Path**|**id**  <br>*required*|ID of the Topology|integer (int32)|


#### Responses

|HTTP Code|Description|Schema|
|---|---|---|
|**200**|Success|No Content|
|**400**|Bad Request|No Content|
|**404**|Not Found|No Content|


#### Tags

* Topologies




<a name="definitions"></a>
## Definitions

<a name="application"></a>
### Application

|Name|Schema|
|---|---|
|**applicationId**  <br>*optional*|integer (int32)|
|**applicationName**  <br>*optional*|string|
|**resourceId**  <br>*optional*|integer (int32)|
|**supportedOperations**  <br>*optional*|< [ApplicationOperation](#applicationoperation) > array|


<a name="applicationoperation"></a>
### ApplicationOperation

|Name|Schema|
|---|---|
|**applicationOperationId**  <br>*optional*|integer (int32)|
|**operationId**  <br>*optional*|integer (int32)|
|**operationName**  <br>*optional*|string|
|**supportedRanges**  <br>*optional*|< [Range](#range) > array|


<a name="moduleconveyor"></a>
### ModuleConveyor

|Name|Schema|
|---|---|
|**moduleConveyorId**  <br>*optional*|integer (int32)|
|**moduleConveyorName**  <br>*optional*|string|
|**plcIpAddressString**  <br>*optional*|string|
|**plcMacAddressString**  <br>*optional*|string|
|**resourceId**  <br>*optional*|integer (int32)|


<a name="range"></a>
### Range

|Name|Schema|
|---|---|
|**id**  <br>*optional*|integer (int32)|
|**name**  <br>*optional*|string|
|**parameters**  <br>*optional*|< [RangeParameter](#rangeparameter) > array|


<a name="rangeparameter"></a>
### RangeParameter

|Name|Schema|
|---|---|
|**defaultValue**  <br>*optional*|number (double)|
|**id**  <br>*optional*|integer (int32)|
|**maximum**  <br>*optional*|number (double)|
|**maximumFunction**  <br>*optional*|string|
|**minimum**  <br>*optional*|number (double)|
|**minimumFunction**  <br>*optional*|string|
|**name**  <br>*optional*|string|


<a name="resource"></a>
### Resource

|Name|Schema|
|---|---|
|**applicationId**  <br>*optional*|integer (int32)|
|**moduleConveyorId**  <br>*optional*|integer (int32)|
|**resourceId**  <br>*optional*|integer (int32)|
|**resourceName**  <br>*optional*|string|


<a name="topology"></a>
### Topology

|Name|Schema|
|---|---|
|**conveyorIds**  <br>*optional*|< integer (int32) > array|
|**conveyorLinkIds**  <br>*optional*|< string, < string, integer (int32) > map > map|
|**isActive**  <br>*optional*|boolean|
|**topologyId**  <br>*optional*|integer (int32)|
|**topologyName**  <br>*optional*|string|





